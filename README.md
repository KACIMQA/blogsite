# Blogsite

Blogging web app for SWE&DevOps module

To run this application's backend with the sample data, please execute the below command, or equivalents depending on your directory structure and operating system

```bash
/bin/python3 /your/path/to/blogsite/src/back_end/app.py
```

and for the front end, please execute the following from src/front_end/:

```bash
npm start
```

Alternatively, dockerfiles exist for building in both the src/back_end and src/front_end directories, with a docker compose file existing in the src directory.
These have only been tested on an x86 Ubuntu 20.04 machine, compatability issues with different devices may occur.

The repository for this system exists at <https://gitlab.com/KACIMQA/blogsite/-/tree/main>.

When the application is operating with the supplied database (i.e. for the coursework submission), an example user has been created with the username ExampleUser and password ExampleUser, with an admin created with the username ExampleAdmin and password ExampleAdmin
