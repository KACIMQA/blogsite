'''Contains configurations for flask variables'''
import os
import random
import string
from datetime import timedelta

BASE_DIR = os.path.dirname(os.path.realpath(__file__))

# Secrets are stored in code, this is not ideal, they should be managed on
# a server and be rotated, however, without a production system and server
# to manage and rotate secrets, this has been ignored, with rotation done
# through randomisation of a value upon startup.


class ApiConfig():
    '''Set flask config inline with flask reccomendations as a class'''
    # Generates a 10 charachter phrase to append to each key
    SECRET_KEY = "flask-qa-secret"+''.join(random.choice(string.ascii_letters) for i in range(10))
    JWT_SECRET_KEY = "jwt-qa-secret"+''.join(random.choice(string.ascii_letters) for i in range(10))
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(hours=1)
