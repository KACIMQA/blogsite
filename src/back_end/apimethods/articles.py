'''This module contains routes and methods for handling user account requests'''
import json

from flask import request, Response
from flask_restx import Resource, fields
from .utils import token_required, decode_jwt

def apply_articles(rest_api, db_man, jwt_block_list):
    '''Generates routes and applies methods for article functionality of API'''

    #Models created to verify route inputs against
    create_model = rest_api.model(
        'CreateModel', {
            "title": fields.String(required=True, min_length=1),
            "content": fields.String(required=True, min_length=1)
            }
        )

    specific_article_model = rest_api.model(
        'SpecificArticleModel', {
            "id": fields.Integer(required=True, min_length=1),
            }
        )

    update_article_model = rest_api.model(
        'UpdateArticleModel', {
            "title": fields.String(required=True, min_length=1),
            "content": fields.String(required=True, min_length=1),
            "id": fields.Integer(required=True, min_length=1)
            }
        )

    #Common responses for shared API rejections
    article_taken_resp = Response(status=400, response=json.dumps(
        {"success": False, "msg": "Article title is already taken."}))

    article_not_exist_resp = Response(status=400, response=json.dumps(
        {"success": False, "msg": "Current article may not exist."}))

    article_user_unallowed_resp = Response(status=400, response=json.dumps(
        {"success": False, "msg": "User does not have permission to edit this post."}))

    #Token_required indicates a method with token validation checks
    @rest_api.route('/articles/update')
    class Update(Resource):
        """
        Article updates methods
        """
        @token_required(jwt_block_list, db_man)
        @rest_api.expect(update_article_model, validate=True)
        def post(self):
            '''post method for updating a specific article'''
            return post_article_update(db_man, article_taken_resp,
                                       article_not_exist_resp, article_user_unallowed_resp)

    @rest_api.route('/articles/delete')
    class Delete(Resource):
        """
        Deletes a specific article
        """
        @token_required(jwt_block_list, db_man)
        @rest_api.expect(specific_article_model, validate=True)
        def post(self):
            '''post method for getting a specific article'''
            return post_article_delete(db_man, article_not_exist_resp,
                                       article_user_unallowed_resp)

    @rest_api.route('/articles/post')
    class RetrieveSingle(Resource):
        """
        Gets a specific article
        """
        @token_required(jwt_block_list, db_man)
        @rest_api.expect(specific_article_model, validate=True)
        def post(self):
            '''post method for getting a specific article'''
            return post_get_specific_articles(db_man)

    @rest_api.route('/articles/')
    class RetrieveMany(Resource):
        """
        Gets a specific article
        """
        @token_required(jwt_block_list, db_man)
        def post(self):
            '''post method for getting a specific article'''
            return post_get_all_articles(db_man)

    @rest_api.route('/articles/create')
    class Create(Resource):
        """
        Creates a new user by taking 'signup_model' input
        """
        @token_required(jwt_block_list, db_man)
        @rest_api.expect(create_model, validate=True)
        def post(self):
            '''post method for creating a article'''
            return post_create_article(db_man, article_taken_resp)

def post_create_article(db_man, article_taken_resp):
    '''post method for creating an article'''

    # Gets input request event
    req_data = request.get_json()

    # Inputs validated for existence by model
    _title = req_data.get("title")
    _content = req_data.get("content")

    # Token already validated by @token_required
    data = decode_jwt(request.cookies.get('authorization'))

    # Check if article title is already taken
    if db_man.retrieve_article_exists(_title):
        return article_taken_resp

    # Pushes to database to create
    db_man.create_article(_title, _content, data["username"])

    # Returns JSON success
    return {"success": True,
            "msg": "Article has ben created."}, 200

def post_get_all_articles(db_man):
    '''post method for creating an article'''
    # User token has already been validated and secured by token required

    # Pulls all articles from database
    articles = db_man.retrieve_all_articles()

    # Returns all articles from database
    return {"success": True,
            "msg": articles}, 200

def post_get_specific_articles(db_man):
    '''post method for creating an article'''
    # User access has already been validated and secured by token required

    # Gets input request event
    req_data = request.get_json()

    # Inputs validated for existence by model
    _id = req_data.get("id")
    # Gets article from database by ID
    article = db_man.retrieve_specific_article(_id)

    # Returns result of database query
    return {"success": True,
            "msg": article}, 200

def post_article_update(db_man, article_taken_resp, article_not_exist_resp,
                        article_user_unallowed_resp):
    '''Post method for updating an article'''
    # Gets input Request event
    req_data = request.get_json()
    # Inputs have already been verified by route model
    _id = req_data.get("id")
    _title = req_data.get("title")
    _content = req_data.get("content")

    # Token already secured and verified by token_required
    data = decode_jwt(request.cookies.get('authorization'))

    # Checks that the user is either an admin or the author of the article
    article_check = check_article(db_man, data['username'], _id,
                                  article_not_exist_resp, article_user_unallowed_resp)

    # Breaks and returns invalidation if the user is unauthorised
    if article_check is not None:
        return article_check

    # Checks if the new title for the article is free
    if db_man.retrieve_article_exists(_title):
        return article_taken_resp

    # Runs update query
    db_man.update_article(_id, _title, _content)
    return {"success": True,
            "msg": "Article has been updated."}, 200

def post_article_delete(db_man, article_not_exist_resp,
                        article_user_unallowed_resp):
    '''Post method for updating an article'''

    # Gets request input event, verified by model
    req_data = request.get_json()
    _id = req_data.get("id")

    # JWT already verified and secured by token_required
    data = decode_jwt(request.cookies.get('authorization'))

    # Checks that the user is allowed to delete the article
    article_check = check_article(db_man, data['username'], _id,
                                  article_not_exist_resp, article_user_unallowed_resp)

    # Breaks and returns if the user isn't allowed to delete the article
    if article_check is not None:
        return article_check

    # Deletes the article, return positive response
    db_man.delete_article(_id)
    return {"success": True,
            "msg": "Article has been deleted."}, 200

def check_article(db_man, _username, _id, article_not_exist_resp,
                  article_user_unallowed_resp):
    '''Shared checks performed by post and update articles'''
    # Checks that the article exists
    if not db_man.retrieve_article_exists_id(_id):
        return article_not_exist_resp

    # Checks that a user is either an admin or the author, XOR.
    if ((db_man.retrieve_article_author(_id) != _username) ^
            (db_man.retrieve_group(_username)[0] == 'admin')):
        return article_user_unallowed_resp

    # Return None in line with pep8
    return None
