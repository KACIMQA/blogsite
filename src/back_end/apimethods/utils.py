'''This module contains utils sharead by api methods'''
from functools import wraps
import json
import jwt

from flask import request, Response
from .config import ApiConfig

def token_required(jwt_block_list, db_man):
    '''
    Decorator functionality for verifying a token, takes the App level token
     block list and a database connection.
    '''
    # Generic response for a bad token
    invalid_token_resp = Response(status=400, response=json.dumps(
        {"success": False, "msg": "Token is invalid/missing, please retry."}))

    def decorator(func):
        '''Defines that a decorator is being created'''
        @wraps(func)
        def wrapper(*args, **kwargs):
            '''Functionality to be ran before function being decorated'''
            try:
                # Gets a token from a request
                token = request.cookies.get('authorization')

                # Decodes the token using the matching key and algorithm
                data = jwt.decode(
                    token,
                    ApiConfig.SECRET_KEY,
                    algorithms=["HS256"])
                # Checks if it's already been blocked or not
                if token in jwt_block_list:
                    return invalid_token_resp

                # Checks if the user actually exists
                if not db_man.retrieve_user_exists(data["username"], None):
                    return invalid_token_resp
            # Catch for all possible combinations of key errors and invalidation errors
            except (KeyError, jwt.InvalidTokenError) as err:
                print(err)
                return invalid_token_resp
            # Runs the function being wrapped if all's gone well
            result = func(*args, **kwargs)
            return result
        return wrapper
    return decorator

def decode_jwt(token):
    '''Decodes a jwt that has already been validated from above'''
    return jwt.decode(
        token,
        ApiConfig.SECRET_KEY,
        algorithms=["HS256"])
