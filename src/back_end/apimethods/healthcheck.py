'''This module contains a blueprint for handling healtheck methods'''
from flask import Blueprint

HEALTH_CHECK_BLUEPRINT = Blueprint("health", __name__)


@HEALTH_CHECK_BLUEPRINT.route('', methods=["GET"])
def health():
    '''Handler for health check method'''
    return health_check()


def health_check():
    '''Returns a simple message; if the app is reachable, this should be returned'''
    return 'Status: up'
