'''This module contains routes and methods for handling user account requests'''
from datetime import datetime, timedelta
import json
import jwt
from flask import request, Response
from flask_restx import Resource, fields
from werkzeug.security import generate_password_hash, check_password_hash

from .utils import token_required, decode_jwt
from .config import ApiConfig


def apply_users(rest_api, db_man, jwt_block_list):
    '''Generates routes and applies methods for users functionality of API'''
    # Max lengths set on fields to help prevent some forms of simple attacks.
    # Models utilised to verify user inputs.
    register_model = rest_api.model(
        'RegisterModel', {
            "username": fields.String(required=True, min_length=2, max_length=32),
            "email": fields.String(required=True, min_length=6),
            "password": fields.String(required=True, min_length=4, max_length=20)
            }
        )

    edit_model = rest_api.model(
        'EditModel', {
            "current_username": fields.String(required=True, min_length=2, max_length=32),
            "new_username": fields.String(required=False, min_length=2, max_length=32),
            "email": fields.String(required=False, min_length=6),
            "current_password": fields.String(required=False, min_length=4, max_length=20),
            "new_password": fields.String(required=False, min_length=4, max_length=20)
            }
        )

    login_model = rest_api.model(
        'LoginModel', {
            "username": fields.String(required=True, min_length=2, max_length=32),
            "password": fields.String(required=True, min_length=4, max_length=20)
            }
        )

    # Generic responses for common failures
    bad_credentials_resp = Response(status=400, response=json.dumps(
        {"success": False, "msg": "Credentials are incorrect, please retry."}))
    user_taken_resp = Response(status=400, response=json.dumps(
        {"success": False, "msg": "Credentials unavailable, please retry."}))

    # token_required verifies user tokens for routes wehere this is required
    @rest_api.route('/users/register')
    class Register(Resource):
        """
        Creates a new user by taking 'signup_model' input
        """

        @rest_api.expect(register_model, validate=True)
        def post(self):
            '''post method for registering a user'''
            return post_register_user(db_man, user_taken_resp)

    @rest_api.route('/users/login')
    class Login(Resource):
        """
        Login user by taking 'login_model' input and return JWT token
        """

        @rest_api.expect(login_model, validate=True)
        def post(self):
            '''post method for logging in a user'''
            return post_login_user(db_man, bad_credentials_resp)

    @rest_api.route('/users/admin')
    class AdminUsers(Resource):
        """
        Gets a list of all users, restricted to admins
        """
        @token_required(jwt_block_list, db_man)
        def post(self):
            '''post method for logging out a user'''
            return post_get_all_users(db_man)


    @rest_api.route('/users/edit')
    class Edit(Resource):
        """
        Edit user by taking 'edit_model' input and JWT. Verifies if users can
        edit the account they're tyring to change before updating.
        """
        @token_required(jwt_block_list, db_man)
        @rest_api.expect(edit_model, validate=True)
        def post(self):
            '''post method for editting a user'''
            return post_edit_user(db_man, bad_credentials_resp, user_taken_resp)

    @rest_api.route('/users/logout')
    class LogoutUser(Resource):
        """
        Logs out a user by adding their JWT to be invalidated
        """
        @token_required(jwt_block_list, db_man)
        def post(self):
            '''post method for logging out a user'''
            return post_logout(jwt_block_list)

def post_register_user(db_man, user_taken_resp):
    '''post method for registering a user'''
    # Gets request event
    req_data = request.get_json()
    # Gets inputs from request event
    _username = req_data.get("username")
    _email = req_data.get("email")
    _password = req_data.get("password")

    # Checks if the user exists
    if db_man.retrieve_user_exists(_username, _email):
        return user_taken_resp

    # Generates a secured and hashed password using pbkdf2:sha256 and a 16
    # character salt
    secured_password = generate_password_hash(_password)

    # Creates a user in the database
    db_man.create_user(_username, _email, secured_password, "user")
    # Returns a success for a successful registration
    return Response(status=200, response=json.dumps(
        {"success": True, "msg": "The user was successfully registered."}))

def post_login_user(db_man, bad_credentials_resp):
    '''post method for logging in a user'''
    # Gets request event
    req_data = request.get_json()
    # Gets inputs from request event
    _username = req_data.get("username")
    _password = req_data.get("password")
    # Checks if user exists, returns negative response if they don't
    if not db_man.retrieve_user_exists(_username, None):
        return bad_credentials_resp
    # Checks if the user has supplied the correct password
    if not check_password_hash(db_man.retrieve_password(_username)[0], _password):
        return bad_credentials_resp

    # Creates access token uwing JWT, expires it after 30 minutes
    token = jwt.encode({'username': _username, 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8')
    # Gets the role of the user
    role = db_man.retrieve_group(_username)[0]
    # Response object, holds message, status code and the role of the user
    response = Response(status=200,
                        response=json.dumps({"success": True, "role":role}))
    # Adds the JWT as a cookie for the response. Authorization key enables this
    # to be sent as the auth token in auth requests by browsers, same life span
    # as the token is set to the cookie. httpOnly prevents JavaScript from
    # accessing the token, preventing XSS. secure enables encryption in
    # transit. samesite prevents third parties websites from making requests
    # utilising the cookie.
    response.set_cookie(
        key="authorization",
        value=token,
        max_age=timedelta(
            minutes=30),
        expires=datetime.utcnow() +
        timedelta(
            minutes=30),
        httponly=True,
        secure=True,
        samesite="Strict")

    # Returns Response object
    return response

def post_edit_user(db_man, bad_credentials_resp, user_taken_resp):
    '''post method for editting a user'''
    # Gets request event
    req_data = request.get_json()

    # Gets inputs from request, inputs validated by model.
    _current_username = req_data.get("current_username")
    _new_username = req_data.get("new_username")
    _email = req_data.get("email")
    _current_password = req_data.get("current_password")
    _new_password = req_data.get("new_password")

    # Sets up a variable for if the password needs updating
    secured_password = None

    # Retrieves username from the JWT
    data = decode_jwt(request.cookies.get('authorization'))

    # Checks if the user is an admin, if so they can overwrite profiles
    if db_man.retrieve_group(data["username"])[0] != "admin":
        # If the user isn't an admin or the user they're trying to edit, the
        # request is rejected.
        if data['username'] != _current_username:
            return bad_credentials_resp
        # If they haven't supplied a password, we cannot verify it's the user.
        if _current_password is None:
            return bad_credentials_resp
        # If the current password is incorrect, they can't edit their profile.
        if not check_password_hash(
                db_man.retrieve_password(_current_username)[0],
                _current_password):
            return bad_credentials_resp

    # Checks if the current user exists in the database
    if not db_man.retrieve_user_exists(
            _current_username, _email):
        return {"success": False,
                "msg": "Account may not exist, please retry."}, 400

    # Checks if the username/email they're trying to take is avaialable
    if db_man.retrieve_user_exists(_new_username, _email):
        return user_taken_resp

    # If no new email is supplied, get their current one
    if _email is None:
        _email = db_man.retrieve_email(_current_username)[0]

    # If no new password is supplied, get their current one, otherwise generate
    # New hash
    if _new_password is None:
        secured_password = db_man.retrieve_password(_current_username)[
            0]
    else:
        secured_password = generate_password_hash(_new_password)
    # If a new username isn't provide, use their current username
    if _new_username is None:
        _new_username = _current_username

    # Updates the user
    db_man.update_user(
        _new_username,
        secured_password,
        _email,
        _current_username)

    # Returns success response
    return {"success": True,
            "msg": "User has been editted", }, 200

def post_logout(jwt_block_list):
    '''post method for logging out a user'''
    #Gets the token from the API request, adds it to the block lsit
    _jwt_token = request.cookies.get('authorization')
    jwt_block_list.append(_jwt_token)

    # Generates successful response object
    response = Response(status=200,
                        response=json.dumps({"success": True}))
    # Overwrites the cookie on the browser so requests that will be auto
    # rejected aren't performed accidently
    response.set_cookie(
        key="authorization",
        httponly=True)

    # Returns response cookie response message
    return response

def post_get_all_users(db_man):
    '''post method for admins getting a list of users'''
    # Token verified by token_required
    data = decode_jwt(request.cookies.get('authorization'))

    # Checks if the user is allowed to perform this action
    if db_man.retrieve_group(data["username"])[0] != "admin":
        return Response(status=400,
                        response=json.dumps({"success": False,
                                             "msg": "User is unauthorised for this action"}))
    # Gets all users
    users = db_man.get_all_users()

    # Returns all users
    return Response(status=200,
                    response=json.dumps({"success": True,
                                         "msg": users}))
