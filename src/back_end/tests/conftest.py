from calendar import c
from werkzeug.security import generate_password_hash
from db.database_manager import DatabaseManager
import pytest
import os

@pytest.fixture(scope="session", autouse=True)
def my_fixture():
    print("Setting up tests...")
    print("deleting existing database...")
    if os.path.exists('blogsDB.sqlite'):
        os.remove('blogsDB.sqlite')
    print("Creating User...")
    db_man = DatabaseManager()
    db_man.create_user("username", "email@email.com",generate_password_hash("password"),"user")
    print("creating edit user")
    db_man.create_user("editname", "edit@edit.com",generate_password_hash("password"),"user")
    print("creating admin")
    db_man.create_user("admin", "admin@admin.com",generate_password_hash("password"),"admin")
    print("creating articles")
    db_man.create_article("Test article 1", "content for test article 1", "username")
    db_man.create_article("Test article 2", "content for test article 2", "username")
    db_man.create_article("Test article 3", "content for test article 3", "username")
    print("Yielding...")
    yield 
    print ('TEAR DOWN')