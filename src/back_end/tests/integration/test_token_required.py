from apimethods.users import token_required
'''Tests for token required decorator, running as integration between api, token required, and logout.'''
#External Modules
import pytest
from apimethods.config import ApiConfig
from datetime import datetime, timedelta
import jwt


#Created Modules
from app import create_app

#Fixture for use within tests, CSRF ignored here as test is on whether app 
#Applies routes, not whether it's secure.
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        yield client

BAD_TOKEN = b'{"success": false, "msg": "Token is invalid/missing, please retry."}'
USER_LOGOUT_ENDPOINT = "/users/logout"
#Most of the testing of the validation of the jwt has been covered in users_logout
def test_token_required(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
        ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.post(USER_LOGOUT_ENDPOINT, )
    assert resp.status_code == 200

def test_token_required_no_token_status(client):
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 400

def test_token_required_no_token_response(client):
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.data == BAD_TOKEN

def test_token_required_expired_token_status(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) - timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 400

def test_token_required_expired_token_response(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) - timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.data == BAD_TOKEN

def test_token_invalid_user_status(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "notauserindb", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 400

def test_token_invalid_user_response(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "notauserindb", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.data == BAD_TOKEN

def test_token_no_user_status(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({ 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 400

def test_token_no_user_response(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({ 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.data == BAD_TOKEN

def test_token_bad_key_status(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({ 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, "BAD_KEY"))
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 400

def test_token_bad_key_response(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({ 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, "BAD_KEY"))
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.data == BAD_TOKEN

def test_token_required_blocked_token(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    client.post(USER_LOGOUT_ENDPOINT)
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.data == BAD_TOKEN

def test_token_required_blocked_token(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    client.post(USER_LOGOUT_ENDPOINT)
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 400