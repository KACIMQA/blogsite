'''Tests for routes being applied to flask app and models being applied. Does not test functionality.'''
#External Modules
from operator import truediv
import pytest
import jwt
from datetime import datetime, timedelta

from apimethods.config import ApiConfig

#Created Modules
from app import create_app

#Fixture for use within tests, CSRF ignored here as test is on whether app 
#Applies routes, not whether it's secure.
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
        ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
        yield client

#Health check tests
HEALTH_ENDPOINT = "/health"
def test_health(client):
    resp = client.get(HEALTH_ENDPOINT)
    assert resp.status_code == 200

def test_health_reject_post(client):
    resp = client.post(HEALTH_ENDPOINT)
    assert resp.status_code == 405

def test_health_reject_put(client):
    resp = client.put(HEALTH_ENDPOINT)
    assert resp.status_code == 405

def test_health_reject_delete(client):
    resp = client.delete(HEALTH_ENDPOINT)
    assert resp.status_code == 405

def test_health_reject_patch(client):
    resp = client.patch(HEALTH_ENDPOINT)
    assert resp.status_code == 405

def test_health_accept_head(client):
    resp = client.head(HEALTH_ENDPOINT)
    assert resp.status_code == 200

def test_health_accept_options(client):
    resp = client.options(HEALTH_ENDPOINT)
    assert resp.status_code == 200

# Articles update tests
ARTICLES_UPDATE_ENDPOINT = "/articles/update"
def test_article_update(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.post(ARTICLES_UPDATE_ENDPOINT, json = {"id":2, "title":"test title update", "content":"test content update"})
    assert resp.status_code == 200

def test_article_update_reject_get(client):
    resp = client.get(ARTICLES_UPDATE_ENDPOINT)
    assert resp.status_code == 405

def test_article_update_reject_put(client):
    resp = client.put(ARTICLES_UPDATE_ENDPOINT)
    assert resp.status_code == 405

def test_article_update_reject_delete(client):
    resp = client.delete(ARTICLES_UPDATE_ENDPOINT)
    assert resp.status_code == 405

def test_article_update_reject_patch(client):
    resp = client.patch(ARTICLES_UPDATE_ENDPOINT)
    assert resp.status_code == 405

def test_article_update_reject_head(client):
    resp = client.head(ARTICLES_UPDATE_ENDPOINT)
    assert resp.status_code == 405

def test_article_update_accept_options(client):
    resp = client.options(ARTICLES_UPDATE_ENDPOINT)
    assert resp.status_code == 200

# Articles delete tests
ARTICLES_DELETE_ENDPOINT = "/articles/delete"
def test_article_delete(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.post(ARTICLES_DELETE_ENDPOINT, json = {"id":3})
    assert resp.status_code == 200

def test_article_delete_reject_get(client):
    resp = client.get(ARTICLES_DELETE_ENDPOINT)
    assert resp.status_code == 405

def test_article_delete_reject_put(client):
    resp = client.put(ARTICLES_DELETE_ENDPOINT)
    assert resp.status_code == 405

def test_article_delete_reject_delete(client):
    resp = client.delete(ARTICLES_DELETE_ENDPOINT)
    assert resp.status_code == 405

def test_article_delete_reject_patch(client):
    resp = client.patch(ARTICLES_DELETE_ENDPOINT)
    assert resp.status_code == 405

def test_article_delete_reject_head(client):
    resp = client.head(ARTICLES_DELETE_ENDPOINT)
    assert resp.status_code == 405

def test_article_delete_accept_options(client):
    resp = client.options(ARTICLES_DELETE_ENDPOINT)
    assert resp.status_code == 200

# Articles posts tests
ARTICLES_POST_ENDPOINT = "/articles/post"
def test_posts_post(client):
    resp = client.post(ARTICLES_POST_ENDPOINT, json={"id":1})
    assert resp.status_code == 200

def test_posts_reject_get(client):
    resp = client.get(ARTICLES_POST_ENDPOINT)
    assert resp.status_code == 405

def test_posts_reject_put(client):
    resp = client.put(ARTICLES_POST_ENDPOINT)
    assert resp.status_code == 405

def test_posts_reject_delete(client):
    resp = client.delete(ARTICLES_POST_ENDPOINT)
    assert resp.status_code == 405

def test_posts_reject_patch(client):
    resp = client.patch(ARTICLES_POST_ENDPOINT)
    assert resp.status_code == 405

def test_posts_reject_head(client):
    resp = client.head(ARTICLES_POST_ENDPOINT)
    assert resp.status_code == 405

def test_posts_accept_options(client):
    resp = client.options(ARTICLES_POST_ENDPOINT)
    assert resp.status_code == 200

# Articles create tests
ARTICLES_CREATE_ENDPOINT = "/articles/create"
def test_article_create(client):
    resp = client.post(ARTICLES_CREATE_ENDPOINT, json={"username":"username", "title":"Test","content":"This is a test article"},
     headers={"authorization":jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8')})
    assert resp.status_code == 200

def test_article_create_reject_get(client):
    resp = client.get(ARTICLES_CREATE_ENDPOINT)
    assert resp.status_code == 405

def test_article_create_reject_put(client):
    resp = client.put(ARTICLES_CREATE_ENDPOINT)
    assert resp.status_code == 405

def test_article_create_reject_delete(client):
    resp = client.delete(ARTICLES_CREATE_ENDPOINT)
    assert resp.status_code == 405

def test_article_create_reject_patch(client):
    resp = client.patch(ARTICLES_CREATE_ENDPOINT)
    assert resp.status_code == 405

def test_article_create_reject_head(client):
    resp = client.head(ARTICLES_CREATE_ENDPOINT)
    assert resp.status_code == 405

def test_article_create_accept_options(client):
    resp = client.options(ARTICLES_CREATE_ENDPOINT)
    assert resp.status_code == 200

# User register tests
USER_REGISTER_ENDPOINT = "/users/register"
def test_user_register(client):
    resp = client.post(USER_REGISTER_ENDPOINT, json={"username":"username2", "email":"email2@email.com","password":"password"})
    assert resp.status_code == 200

def test_user_register_bad_json(client):
    resp = client.post(USER_REGISTER_ENDPOINT)
    assert resp.status_code == 400

def test_user_register_reject_get(client):
    resp = client.get(USER_REGISTER_ENDPOINT)
    assert resp.status_code == 405

def test_user_register_reject_put(client):
    resp = client.put(USER_REGISTER_ENDPOINT)
    assert resp.status_code == 405

def test_user_register_reject_delete(client):
    resp = client.delete(USER_REGISTER_ENDPOINT)
    assert resp.status_code == 405

def test_user_register_reject_patch(client):
    resp = client.patch(USER_REGISTER_ENDPOINT)
    assert resp.status_code == 405

def test_user_register_reject_head(client):
    resp = client.head(USER_REGISTER_ENDPOINT)
    assert resp.status_code == 405

def test_user_register_accept_options(client):
    resp = client.options(USER_REGISTER_ENDPOINT)
    assert resp.status_code == 200

# User login tests
USER_LOGIN_ENDPOINT = "/users/login"
def test_user_login(client):
    # Required to create an account before it can be signed in, account creation tested above
    resp = client.post(USER_LOGIN_ENDPOINT, json={"username":"username", "password":"password"})
    assert resp.status_code == 200

def test_user_login_no_account(client):
    resp = client.post(USER_LOGIN_ENDPOINT, json={"username":"username1", "password":"password"})
    assert resp.status_code == 400

def test_user_login_bad_json(client):
    # Required to create an account before it can be signed in, account creation tested above
    resp = client.post(USER_LOGIN_ENDPOINT)
    assert resp.status_code == 400

def test_user_login_reject_get(client):
    resp = client.get(USER_LOGIN_ENDPOINT)
    assert resp.status_code == 405

def test_user_login_reject_put(client):
    resp = client.put(USER_LOGIN_ENDPOINT)
    assert resp.status_code == 405

def test_user_login_reject_delete(client):
    resp = client.delete(USER_LOGIN_ENDPOINT)
    assert resp.status_code == 405

def test_user_login_reject_patch(client):
    resp = client.patch(USER_LOGIN_ENDPOINT)
    assert resp.status_code == 405

def test_user_login_reject_head(client):
    resp = client.head(USER_LOGIN_ENDPOINT)
    assert resp.status_code == 405

def test_user_login_accept_options(client):
    resp = client.options(USER_LOGIN_ENDPOINT)
    assert resp.status_code == 200

# User logout tests
USER_LOGOUT_ENDPOINT = "/users/logout"
def test_user_logout(client):
    # Required to create an account before it can be signed in, account creation tested above
    client.post(USER_REGISTER_ENDPOINT, json={"username":"username", "email":"email@email.com","password":"password"})
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 200

def test_user_logout_reject_get(client):
    resp = client.get(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 405

def test_user_logout_reject_put(client):
    resp = client.put(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 405

def test_user_logout_reject_delete(client):
    resp = client.delete(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 405

def test_user_logout_reject_patch(client):
    resp = client.patch(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 405

def test_user_logout_reject_head(client):
    resp = client.head(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 405

def test_user_logout_accept_options(client):
    resp = client.options(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 200

# User edit tests
USERS_ADMINSTRATION_ENDPOINT = "/users/admin"
def test_user_edit(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "admin", 'exp': datetime.utcnow(
        ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.post(USERS_ADMINSTRATION_ENDPOINT)
    assert resp.status_code == 200

def test_user_edit_no_jwt(client):
    resp = client.post(USERS_ADMINSTRATION_ENDPOINT)
    print(resp.data)
    assert resp.status_code == 400

def test_user_edit_reject_get(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "admin", 'exp': datetime.utcnow(
        ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.get(USERS_ADMINSTRATION_ENDPOINT)
    assert resp.status_code == 405

def test_user_edit_reject_put(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "admin", 'exp': datetime.utcnow(
        ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.put(USERS_ADMINSTRATION_ENDPOINT)
    assert resp.status_code == 405

def test_user_edit_reject_delete(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "admin", 'exp': datetime.utcnow(
        ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.delete(USERS_ADMINSTRATION_ENDPOINT)
    assert resp.status_code == 405

def test_user_edit_reject_patch(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "admin", 'exp': datetime.utcnow(
        ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.patch(USERS_ADMINSTRATION_ENDPOINT)
    assert resp.status_code == 405

def test_user_edit_reject_head(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "admin", 'exp': datetime.utcnow(
        ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.head(USERS_ADMINSTRATION_ENDPOINT)
    assert resp.status_code == 405

def test_user_edit_accept_options(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "admin", 'exp': datetime.utcnow(
        ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.options(USERS_ADMINSTRATION_ENDPOINT)
    assert resp.status_code == 200

# User edit tests
USERS_EDIT_ENDPOINT = "/users/edit"
def test_user_edit(client):
    # Required to create an account before it can be signed in, account creation tested above
    client.post(USER_REGISTER_ENDPOINT, json={"username":"username2", "email":"email2@email.com","password":"password"})
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username2", 'exp': datetime.utcnow(
        ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8') )
    resp = client.post(USERS_EDIT_ENDPOINT,
    json={"current_username":"username2", "new_username":"bob", "current_password":"password"})
    print(resp.data)
    assert resp.status_code == 200

def test_user_edit_no_json(client):
    resp = client.post(USERS_EDIT_ENDPOINT, headers={"authorization":jwt.encode({'username': "username2", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8')}
    )
    print(resp.data)
    assert resp.status_code == 400

def test_user_edit_no_jwt(client):
    # Required to create an account before it can be signed in, account creation tested above
    resp = client.post(USERS_EDIT_ENDPOINT, 
    json={"current_username":"username2", "new_username":"bob", "current_password":"password"})
    print(resp.data)
    assert resp.status_code == 400

def test_user_edit_reject_get(client):
    resp = client.get(USERS_EDIT_ENDPOINT)
    assert resp.status_code == 405

def test_user_edit_reject_put(client):
    resp = client.put(USERS_EDIT_ENDPOINT)
    assert resp.status_code == 405

def test_user_edit_reject_delete(client):
    resp = client.delete(USERS_EDIT_ENDPOINT)
    assert resp.status_code == 405

def test_user_edit_reject_patch(client):
    resp = client.patch(USERS_EDIT_ENDPOINT)
    assert resp.status_code == 405

def test_user_edit_reject_head(client):
    resp = client.head(USERS_EDIT_ENDPOINT)
    assert resp.status_code == 405

def test_user_edit_accept_options(client):
    resp = client.options(USERS_EDIT_ENDPOINT)
    assert resp.status_code == 200