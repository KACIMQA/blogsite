'''Tests for Login API. Black box tests.'''
#External Modules
import pytest
from werkzeug.datastructures import Headers

#Created Modules
from app import create_app

#Fixture for use within tests, CSRF ignored here as test is on whether app 
#Applies routes, not whether it's secure.
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        yield client

USER_LOGIN_ENDPOINT = "/users/login"
USER_REGISTER_ENDPOINT = "/users/register"

def test_user_login(client):
    resp = client.post(USER_LOGIN_ENDPOINT, json={"username":"username", "password":"password"})
    assert resp.status_code == 200

def test_user_login_sets_cookie(client):
    resp = client.post(USER_LOGIN_ENDPOINT, json={"username":"username", "password":"password"})
    assert resp.headers["Set-Cookie"] != None

def test_user_login_data(client):
    resp = client.post(USER_LOGIN_ENDPOINT, json={"username":"username", "password":"password"})
    assert resp.data == b'{"success": true, "role": "user"}'

def test_user_login_bad_creds(client):
    resp = client.post(USER_LOGIN_ENDPOINT, json={"username":"username", "password":"beans"})
    assert resp.status_code == 400

def test_user_login_bad_creds_data(client):
    resp = client.post(USER_LOGIN_ENDPOINT, json={"username":"username", "password":"beans"})
    assert resp.data == b'{"success": false, "msg": "Credentials are incorrect, please retry."}'

def test_user_login_bad_creds_cookie(client):
    resp = client.post(USER_LOGIN_ENDPOINT, json={"username":"username", "password":"beans"})
    assert resp.headers == Headers([('Content-Type', 'text/html; charset=utf-8'), ('Content-Length', '69')])

def test_user_login_non_existent_user(client):
    resp = client.post(USER_LOGIN_ENDPOINT, json={"username":"doesnotexist", "password":"password"})
    assert resp.status_code == 400

def test_user_login_non_existent_user_data(client):
    resp = client.post(USER_LOGIN_ENDPOINT, json={"username":"doesnotexist", "password":"password"})
    assert resp.data == b'{"success": false, "msg": "Credentials are incorrect, please retry."}'

def test_user_login_non_existent_useer_cookie(client):
    resp = client.post(USER_LOGIN_ENDPOINT, json={"username":"doesnotexist", "password":"password"})
    assert resp.headers == Headers([('Content-Type', 'text/html; charset=utf-8'), ('Content-Length', '69')])
