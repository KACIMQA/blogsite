'''Tests for Login API. Black box tests.'''
#External Modules
import pytest
import json
from datetime import datetime, timedelta
from apimethods.config import ApiConfig
import jwt


#Created Modules
from app import create_app

#Fixture for use within tests, CSRF ignored here as test is on whether app 
#Applies routes, not whether it's secure.
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
        yield client

# Articles create tests ran prior to this suite, articles 3-5 are 
# free for deletion due to not being used in other suites
ARTICLE_DELETE_ENDPOINT = "/articles/delete"

def test_article_delete_status(client):
    resp = client.post(ARTICLE_DELETE_ENDPOINT, json={"id":3})
    assert resp.status_code == 200

def test_article_delete_data(client):
    resp = client.post(ARTICLE_DELETE_ENDPOINT, json={"id":4})
    assert resp.data == b'{"success": true, "msg": "Article has been deleted."}\n'

# editname should not be changed at this point
def test_article_delete_bad_user_status(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(ARTICLE_DELETE_ENDPOINT, json={"id":5})
    assert resp.status_code == 400

def test_article_delete_bad_user_response(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(ARTICLE_DELETE_ENDPOINT, json={"id":5})
    assert resp.data == b'{"success": false, "msg": "User does not have permission to edit this post."}'

def test_article_delete_admin_status(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "admin", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(ARTICLE_DELETE_ENDPOINT, json={"id":5})
    assert resp.status_code == 200

def test_article_delete_unexist_article_status(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "admin", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(ARTICLE_DELETE_ENDPOINT, json={"id":5})
    assert resp.status_code == 400

def test_article_delete_unexist_article_response(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "admin", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(ARTICLE_DELETE_ENDPOINT, json={"id":5})
    assert resp.data == b'{"success": false, "msg": "Current article may not exist."}'
