'''Tests for Login API. Black box tests.'''
#External Modules
import pytest
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
from apimethods.config import ApiConfig
import jwt


#Created Modules
from app import create_app

#Fixture for use within tests, CSRF ignored here as test is on whether app 
#Applies routes, not whether it's secure.
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
        yield client

TEST_CONTENT = { "title":"creation_test_article", "content":"Test article for creation tests"}
ARTICLE_CREATE_ENDPOINT = "/articles/create"
def test_article_create_status(client):
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json=TEST_CONTENT,  )
    assert resp.status_code == 200

def test_article_create_data(client):
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json={ "title":"creation_test_article2", "content":"2nd test article for article creation tests"},  )    
    assert resp.data == b'{"success": true, "msg": "Article has ben created."}\n'

def test_article_create_repeated_title_status(client):
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json=TEST_CONTENT,  )    
    assert resp.status_code == 400

def test_article_create_repeated_title_body(client):
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json=TEST_CONTENT,  )    
    assert resp.data == b'{"success": false, "msg": "Article title is already taken."}'

