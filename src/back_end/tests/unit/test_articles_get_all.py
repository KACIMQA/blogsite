'''Tests for Login API. Black box tests.'''
#External Modules
import pytest
import ast
import json
from datetime import datetime, timedelta
from apimethods.config import ApiConfig
import jwt


#Created Modules
from app import create_app

#Fixture for use within tests, CSRF ignored here as test is on whether app 
#Applies routes, not whether it's secure.
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
        yield client

ARTICLE_RETRIEVE_ENDPOINT = "/articles/"
def test_article_retrieve_status(client):
    resp = client.post(ARTICLE_RETRIEVE_ENDPOINT)
    assert resp.status_code == 200

# Data is being returend by SQL, 5 articles exist from test setups and previous
# Test cases, shouldn't test teh data inside since that's SQLite's function,
# Not something that has been written.
def test_article_retrieve_data(client):
    resp = client.post(ARTICLE_RETRIEVE_ENDPOINT) 
    json_data = json.loads(resp.data)
    print(json_data)
    
    assert len(json_data["msg"]) == 2
