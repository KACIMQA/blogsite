'''Tests for Login API. Black box tests.'''
#External Modules
import pytest
from apimethods.config import ApiConfig
from datetime import datetime, timedelta
import jwt


#Created Modules
from app import create_app

#Fixture for use within tests, CSRF ignored here as test is on whether app 
#Applies routes, not whether it's secure.
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        client.set_cookie("localhost", "authorization", jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
        yield client

USER_LOGOUT_ENDPOINT = "/users/logout"
#Most of the testing of the validation of the jwt has been covered in users_logout
def test_user_logout(client):
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 200

def test_user_logout_data(client):
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.data == b'{"success": true}'

def test_user_logout_sets_cookie(client):
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.headers["Set-Cookie"] != None

def test_user_logout_double_logout(client):
    client.post(USER_LOGOUT_ENDPOINT)
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.status_code == 400


def test_user_logout_double_logout_data(client):
    client.post(USER_LOGOUT_ENDPOINT)
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert resp.data == b'{"success": false, "msg": "Token is invalid/missing, please retry."}'

def test_user_logout_double_logout_cookie(client):
    client.post(USER_LOGOUT_ENDPOINT)
    resp = client.post(USER_LOGOUT_ENDPOINT)
    assert "Set-Cookie" not in resp.headers

