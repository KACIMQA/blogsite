'''Tests for Login API. Black box tests.'''
#External Modules
import pytest
from werkzeug.security import generate_password_hash, check_password_hash


#Created Modules
from app import create_app

#Fixture for use within tests, CSRF ignored here as test is on whether app 
#Applies routes, not whether it's secure.
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        yield client

USER_REGISTER_ENDPOINT = "/users/register"
REGISTER_EMAIL = "register@test.com"
BAD_RESP = b'{"success": false, "msg": "Credentials unavailable, please retry."}'

def test_user_register(client):
    resp = client.post(USER_REGISTER_ENDPOINT, json={"username":"register_test","email":REGISTER_EMAIL, "password":"password"})
    assert resp.status_code == 200

def test_user_register_data(client):
    resp = client.post(USER_REGISTER_ENDPOINT, json={"username":"dif_register_test","email":"dif_register@test.com", "password":"password"})
    assert resp.data == b'{"success": true, "msg": "The user was successfully registered."}'

def test_user_register_taken_user_and_email(client):
    resp = client.post(USER_REGISTER_ENDPOINT, json={"username":"register_test","email":REGISTER_EMAIL, "password":"password"})
    assert resp.status_code == 400

def test_user_register_taken_user_and_email_data(client):
    resp = client.post(USER_REGISTER_ENDPOINT, json={"username":"register_test","email":REGISTER_EMAIL, "password":"password"})
    assert resp.data == BAD_RESP

def test_user_register_taken_email(client):
    resp = client.post(USER_REGISTER_ENDPOINT, json={"username":"new_register_test","email":REGISTER_EMAIL, "password":"password"})
    assert resp.status_code == 400

def test_user_register_taken_email_data(client):
    resp = client.post(USER_REGISTER_ENDPOINT, json={"username":"new_register_test","email":REGISTER_EMAIL, "password":"password"})
    assert resp.data == BAD_RESP

def test_user_register_taken_user(client):
    resp = client.post(USER_REGISTER_ENDPOINT, json={"username":"register_test","email":"new_register@test.com", "password":"password"})
    assert resp.status_code == 400

def test_user_register_taken_user_data(client):
    resp = client.post(USER_REGISTER_ENDPOINT, json={"username":"register_test","email":"new_register@test.com", "password":"password"})
    assert resp.data == BAD_RESP