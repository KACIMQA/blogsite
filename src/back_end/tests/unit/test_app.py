'''Tests for methods in main (app.py)'''
#External modules
import pytest

from flask import Flask
#Moduels being tested
from app import create_app

def test_create_app():
    app = create_app()

    assert isinstance(app, Flask) == True

def test_app_jwt_secret_key_presence():
    app = create_app()

    assert app.config['JWT_SECRET_KEY'] != None
    
def test_app_jwt_expiration_presence():
    app = create_app()

    assert app.config['JWT_ACCESS_TOKEN_EXPIRES'] != None

def test_app_secret_key_presence():
    app = create_app()

    assert app.config['SECRET_KEY'] != None

