'''Tests for Users API. Black box tests.'''
#External Modules
import pytest
import json
from datetime import datetime, timedelta
from apimethods.config import ApiConfig
import jwt


#Created Modules
from app import create_app

#Fixture for use within tests, CSRF ignored here as test is on whether app 
#Applies routes, not whether it's secure.
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        client.set_cookie('localhost', 'authorization',jwt.encode({'username': "admin", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
        yield client

USERS_ADMIN_ENDPOINT = "/users/admin"
def test_users_admin_status(client):
    resp = client.post(USERS_ADMIN_ENDPOINT)
    assert resp.status_code == 200


def test_users_admin_data_count(client):
    resp = client.post(USERS_ADMIN_ENDPOINT) 
    json_data = json.loads(resp.data)
    
    assert len(json_data["msg"]) == 4

def test_users_admin_data_content(client):
    resp = client.post(USERS_ADMIN_ENDPOINT) 
    json_data = json.loads(resp.data)
    
    assert json_data["msg"][0][0]=='username'
    assert json_data["msg"][0][1]=='email@email.com'

def test_users_admin_not_admin_status(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USERS_ADMIN_ENDPOINT)
    assert resp.status_code == 400


def test_users_admin_not_admin_resp(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USERS_ADMIN_ENDPOINT) 
    
    assert resp.data == b'{"success": false, "msg": "User is unauthorised for this action"}'
