'''Tests for Login API. Black box tests.'''
#External Modules
import pytest
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
from apimethods.config import ApiConfig
import jwt


#Created Modules
from app import create_app
from db.database_manager import DatabaseManager

#Fixture for use within tests, CSRF ignored here as test is on whether app 
#Applies routes, not whether it's secure.
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        client.set_cookie('localhost', 'authorization',jwt.encode({'username': "username", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
        yield client

REPEATED_TITLE= "Test title update"
NON_EXISTING_TITLE = "Non Existing Title"
ARTICLE_CREATE_ENDPOINT = "/articles/update"
REPEATED_CONTENT = "test content update"
def test_article_create_status(client):
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":1, "title": REPEATED_TITLE, "content": REPEATED_CONTENT},  )
    assert resp.status_code == 200

def test_article_create_data(client):
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":1, "title": "Test title update2", "content": REPEATED_CONTENT},  )
    assert resp.data == b'{"success": true, "msg": "Article has been updated."}\n'

def test_article_updated_db(client):
    db_man = DatabaseManager()
    pre_change = db_man.retrieve_specific_article(1)
    client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":1, "title": REPEATED_TITLE, "content": "Some new content"},  )
    post_change = db_man.retrieve_specific_article(1)
    print(pre_change)
    print(post_change)
    assert post_change[0] == pre_change[0]
    assert post_change[0] == 1
    assert post_change[1] == REPEATED_TITLE
    assert post_change[1] != pre_change[1]
    assert post_change[3] == "Some new content"
    assert post_change[3] != pre_change[3]
    assert post_change[2] != pre_change[2]
    assert post_change[4] == pre_change[4]
    assert post_change[4] == "username"

def test_article_update_title_taken_status(client):
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":1, "title": REPEATED_TITLE, "content": REPEATED_CONTENT},  )
    assert resp.status_code == 400

def test_article_update_title_taken_data(client):
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":1, "title": REPEATED_TITLE, "content": REPEATED_CONTENT},  )
    assert resp.data == b'{"success": false, "msg": "Article title is already taken."}'

def test_article_update_title_taken_not_update(client):
    db_man = DatabaseManager()
    pre_change = db_man.retrieve_specific_article(1)
    client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":1, "title": REPEATED_TITLE, "content": REPEATED_CONTENT},  )
    post_change = db_man.retrieve_specific_article(1)
    assert post_change == pre_change

def test_article_update_user_mismatch_status(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":1, "title": REPEATED_TITLE, "content": REPEATED_CONTENT},  )
    assert resp.status_code == 400

def test_article_update_user_mismatch_data(client):
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":1, "title": REPEATED_TITLE, "content": REPEATED_CONTENT},  )
    assert resp.data == b'{"success": false, "msg": "User does not have permission to edit this post."}'

def test_article_update_user_mismatch_data_not_update(client):
    db_man = DatabaseManager()
    pre_change = db_man.retrieve_specific_article(1)
    client.set_cookie('localhost', 'authorization',jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":1, "title": REPEATED_TITLE, "content": REPEATED_CONTENT},  )
    post_change = db_man.retrieve_specific_article(1)
    assert post_change == pre_change

def test_article_update_article_unexist_status(client):
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":333, "title": NON_EXISTING_TITLE, "content": REPEATED_CONTENT},  )
    assert resp.status_code == 400

def test_article_update_article_unexist_data(client):
    resp = client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":333, "title": NON_EXISTING_TITLE, "content": REPEATED_CONTENT},  )
    assert resp.data == b'{"success": false, "msg": "Current article may not exist."}'

def test_article_update_article_unexist_data_not_update(client):
    db_man = DatabaseManager()
    pre_change = db_man.retrieve_specific_article(1)
    client.post(ARTICLE_CREATE_ENDPOINT,  json={"id":333, "title": NON_EXISTING_TITLE, "content": REPEATED_CONTENT},  )
    post_change = db_man.retrieve_specific_article(1)
    assert post_change == pre_change