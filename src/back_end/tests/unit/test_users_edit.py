'''Tests for Login API. Black box tests.'''
#External Modules
from calendar import day_abbr
import pytest
from datetime import datetime, timedelta
import jwt
from apimethods.config import ApiConfig
from werkzeug.security import check_password_hash


#Created Modules
from app import create_app
from db.database_manager import DatabaseManager

#Fixture for use within tests, CSRF ignored here as test is on whether app 
#Applies routes, not whether it's secure.
@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False
    with app.test_client() as client:
        yield client

USER_EDIT_ENDPOINT = "/users/edit"
SUCCESSFUL_RESP = b'{"success": true, "msg": "User has been editted"}\n'
BAD_RESP = b'{"success": false, "msg": "Credentials are incorrect, please retry."}'
NEW_EMAIL4 = "new_email4@email.com"
def test_user_edit_min_status(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"password"})
    print(resp.data)
    assert resp.status_code == 200

def test_user_edit_min_no_change(client):
    db_man = DatabaseManager()
    pre_edit = db_man.get_user_details("editname")
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"password"})
    post_edit = db_man.get_user_details("editname")
    assert pre_edit == post_edit

def test_user_edit_min_body(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"password"}, headers={"authorization":jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8')})
    assert resp.data == SUCCESSFUL_RESP

def test_user_edit_bad_pwd_status(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"notpassword"})
    print(resp.data)
    assert resp.status_code == 400

def test_user_edit_bad_pwd_change(client):
    db_man = DatabaseManager()
    pre_edit = db_man.get_user_details("editname")
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"notpassword"})
    post_edit = db_man.get_user_details("editname")
    assert pre_edit == post_edit

def test_user_edit_bad_pwd_body(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"notpassword"})
    assert resp.data == BAD_RESP

def test_user_edit_taken_user_status(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"password", "new_username":"username"})
    print(resp.data)
    assert resp.status_code == 400

def test_user_edit_taken_user_change(client):
    db_man = DatabaseManager()
    pre_edit_orig = db_man.get_user_details("editname")
    pre_edit_new = db_man.get_user_details("username")
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"password", "new_username":"username"})
    post_edit_orig = db_man.get_user_details("editname")
    post_edit_new = db_man.get_user_details("username")
    assert pre_edit_orig == post_edit_orig
    assert pre_edit_new == post_edit_new

def test_user_edit_taken_user_body(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"password", "new_username":"username"})
    assert resp.data == b'{"success": false, "msg": "Credentials unavailable, please retry."}'

def test_user_edit_no_pwd_status(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname"})
    print(resp.data)
    assert resp.status_code == 400

def test_user_edit_no_pwd_change(client):
    db_man = DatabaseManager()
    pre_edit = db_man.get_user_details("editname")
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname"})
    post_edit = db_man.get_user_details("editname")
    assert pre_edit == post_edit

def test_user_edit_no_pwd_body(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname"})
    assert resp.data == BAD_RESP

def test_user_edit_admin_min_status(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "admin", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"password"})
    print(resp.data)
    assert resp.status_code == 200

def test_user_edit_admin_min_no_change(client):
    db_man = DatabaseManager()
    pre_edit = db_man.get_user_details("editname")
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "admin", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"password"})
    post_edit = db_man.get_user_details("editname")
    assert pre_edit == post_edit

def test_user_edit_admin_min_body(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "admin", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"password"})
    assert resp.data == SUCCESSFUL_RESP

def test_user_edit_user_not_exist_status(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "admin", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"unexist"})
    print(resp.data)
    assert resp.status_code == 400

def test_user_edit_not_exist_no_change(client):
    db_man = DatabaseManager()
    pre_edit = db_man.get_user_details("unexist")
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "admin", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    client.post(USER_EDIT_ENDPOINT, json={"current_username":"unexist"})
    post_edit = db_man.get_user_details("unexist")
    assert pre_edit == post_edit

def test_user_edit_not_exist_body(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "admin", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"unexist"})
    assert resp.data == b'{"success": false, "msg": "Account may not exist, please retry."}\n'

def test_user_edit_not_themself_status(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"username"})
    print(resp.data)
    assert resp.status_code == 400

def test_user_edit_not_themself_no_change(client):
    db_man = DatabaseManager()
    pre_edit = db_man.get_user_details("username")
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    client.post(USER_EDIT_ENDPOINT, json={"current_username":"username"})
    post_edit = db_man.get_user_details("username")
    assert pre_edit == post_edit

def test_user_edit_not_themself_body(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"username", "current_password":"password"})
    assert resp.data == BAD_RESP

def test_user_edit_all_fields_status(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname", "current_password":"password", "email":"new_email@email.com", "new_username":"editname2", "new_password":"password2"})
    print(resp.data)
    assert resp.status_code == 200

def test_user_edit_all_fields_change(client):
    db_man = DatabaseManager()
    pre_edit = db_man.get_user_details("editname2")
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname2", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname2", "current_password":"password2", "email":"edit_email3@email.com", "new_username":"editname3", "new_password":"password3"})
    post_edit_old_name = db_man.get_user_details("editname2")
    post_edit_new_name = db_man.get_user_details("editname3")
    assert pre_edit != post_edit_old_name
    assert pre_edit[0] == post_edit_new_name[0]
    assert pre_edit[1] != post_edit_new_name[1]
    assert pre_edit[2] != post_edit_new_name[2]
    assert pre_edit[3] != post_edit_new_name[3]
    assert post_edit_new_name[1] == "editname3"
    assert post_edit_new_name[2] == "edit_email3@email.com"
    assert check_password_hash(post_edit_new_name[3], "password3")

def test_user_edit_all_fields_body(client):
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname3", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname3", "current_password":"password3", "email":NEW_EMAIL4, "new_username":"editname4", "new_password":"password4"})
    assert resp.data == SUCCESSFUL_RESP
#Below tests combine tests from above in one stage for each optional field
#This is not optimal, however, as this is tightly coupleded, if part of the test fails, all the rest should as well.

def test_user_edit_username_change(client):
    db_man = DatabaseManager()
    pre_edit = db_man.get_user_details("editname4")
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname4", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname4", "current_password":"password4",  "new_username":"editname5"})
    post_edit_old_name = db_man.get_user_details("editname4")
    post_edit_new_name = db_man.get_user_details("editname5")
    assert pre_edit != post_edit_old_name
    assert pre_edit[0] == post_edit_new_name[0]
    assert pre_edit[1] != post_edit_new_name[1]
    assert pre_edit[2] == post_edit_new_name[2]
    assert pre_edit[3] == post_edit_new_name[3]
    assert post_edit_new_name[1] == "editname5"
    assert post_edit_new_name[2] == NEW_EMAIL4
    assert check_password_hash(post_edit_new_name[3], "password4")
    assert resp.data == SUCCESSFUL_RESP
    assert resp.status_code == 200

def test_user_edit_password_change(client):
    db_man = DatabaseManager()
    pre_edit = db_man.get_user_details("editname5")
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname5", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname5", "current_password":"password4",  "new_password":"password5"})
    post_edit = db_man.get_user_details("editname5")
    assert pre_edit != post_edit
    assert pre_edit[0] == post_edit[0]
    assert pre_edit[1] == post_edit[1]
    assert pre_edit[2] == post_edit[2]
    assert pre_edit[3] != post_edit[3]
    assert post_edit[1] == "editname5"
    assert post_edit[2] == NEW_EMAIL4
    assert check_password_hash(post_edit[3], "password5")
    assert resp.data == SUCCESSFUL_RESP
    assert resp.status_code == 200

def test_user_edit_email_change(client):
    db_man = DatabaseManager()
    pre_edit = db_man.get_user_details("editname5")
    client.set_cookie('localhost', 'authorization', jwt.encode({'username': "editname5", 'exp': datetime.utcnow(
    ) + timedelta(minutes=30)}, ApiConfig.SECRET_KEY).decode('utf-8'))
    resp = client.post(USER_EDIT_ENDPOINT, json={"current_username":"editname5", "current_password":"password5",  "email":"new_email5@email.com"})
    post_edit = db_man.get_user_details("editname5")
    assert pre_edit != post_edit
    assert pre_edit[0] == post_edit[0]
    assert pre_edit[1] == post_edit[1]
    assert pre_edit[2] != post_edit[2]
    assert pre_edit[3] == post_edit[3]
    assert post_edit[1] == "editname5"
    assert post_edit[2] == "new_email5@email.com"
    assert check_password_hash(post_edit[3], "password5")
    assert resp.data == SUCCESSFUL_RESP
    assert resp.status_code == 200
