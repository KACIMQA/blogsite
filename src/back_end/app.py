'''
app.py operates as the main file for this project,
including the functionality to start an app and then run it
'''

from flask import Flask
from flask_cors import CORS
from flask_restx import Api


from apimethods.healthcheck import HEALTH_CHECK_BLUEPRINT
from apimethods.articles import apply_articles
from apimethods.users import apply_users
from db.database_manager import DatabaseManager


def create_app():
    '''Creates a flask app to be served as an API'''

    # creates the database and connections
    db_man = DatabaseManager()
    jwt_block_list = []
    app = Flask(__name__)

    with app.app_context():
        # Applys the config for the application from the config class
        app.config.from_object('apimethods.config.ApiConfig')

        # Manages CORS through requiring authorization
        CORS(app, supports_credentials=True)

        # Creates an API instance
        rest_api = Api(version="1.0", title="Blogsite API")

        # Applies relevant routes and registers a blueprint for the health check
        apply_users(rest_api, db_man, jwt_block_list)
        apply_articles(rest_api, db_man, jwt_block_list)
        app.register_blueprint(HEALTH_CHECK_BLUEPRINT, url_prefix="/health")

        # Applies the rest_api to the app
        rest_api.init_app(app, add_specs=False)
    return app


if __name__ == "__main__":
    # Initialise app
    APP = create_app()

    # Execute app on localhost:5000
    APP.run(debug=True, host="0.0.0.0", port=5000)
