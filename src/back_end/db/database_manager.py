'''Module contains a class for interacting with and managing database'''
import sqlite3
import datetime

class DatabaseManager:
    '''Manages sqlite interactions and functions'''
    def __init__(self, ):
        self.db_name = 'blogsDB.sqlite'
        self.conn = None
        self.curr = None
        self.open_database()
        self.create_tables()

    def open_database(self):
        '''Opens a connection to an sqlite database'''
        self.conn = sqlite3.connect(self.db_name, check_same_thread=False)
        self.curr = self.conn.cursor()

    def create_tables(self):
        '''Creates tables for functions if they don't exist'''
        self.curr.execute(''' CREATE TABLE IF NOT EXISTS users (
                                        id integer PRIMARY KEY,
                                        user_name text NOT NULL UNIQUE,
                                        email text NOT NULL UNIQUE,
                                        password_hash text NOT NULL,
                                        user_group text NOT NULL
                                    ); ''')

        self.curr.execute(''' CREATE TABLE IF NOT EXISTS articles (
                                        id integer PRIMARY KEY,
                                        title text NOT NULL UNIQUE,
                                        content text NOT NULL,
                                        postchanged timestamp NOT NULL
                                    ); ''')

        self.curr.execute(''' CREATE TABLE IF NOT EXISTS posts (
                                        user_id integer NOT NULL references users(id),
                                        article_id integer NOT NULL UNIQUE references articles(id),
                                        PRIMARY KEY (user_id, article_id)
                                    ); ''')
        self.conn.commit()

    def retrieve_user_exists(self, username, email):
        '''Checks if a provided user name or email exists in the database'''
        for _ in self.curr.execute(''' SELECT 1
                                        FROM users
                                        WHERE user_name = ?
                                        OR email = ?;''',
                                   (username, email)):
            return True
        return False

    def create_user(self, username, email, password, user_group):
        '''Creates a user in the database'''
        self.curr.execute(
            ''' INSERT into users(user_name, email, password_hash, user_group)
                              VALUES(?,?,?,?);''', (username, email, password, user_group))
        self.conn.commit()

    def retrieve_password(self, username):
        '''Gets the password hash of a provided user'''
        pwd_row = self.curr.execute(''' SELECT password_hash
                                        FROM users
                                        WHERE user_name = ?;''',
                                    (username, )).fetchone()
        return pwd_row

    def retrieve_group(self, username):
        '''Gets the group of a provided user'''
        group_row = self.curr.execute(''' SELECT user_group
                                        FROM users
                                        WHERE user_name = ?;''',
                                      (username, )).fetchone()

        return group_row

    def retrieve_email(self, username):
        '''Gets the email of a provided user'''
        email_row = self.curr.execute(''' SELECT email
                                        FROM users
                                        WHERE user_name = ?;''',
                                      (username, )).fetchone()

        return email_row

    def update_user(self, new_username, password, email, current_username):
        '''Updates the details of a provided user'''
        self.curr.execute(''' UPDATE users
                              SET user_name =?,
                              password_hash = ?,
                              email = ?
                              WHERE user_name = ?;''',
                          (new_username, password, email, current_username))
        self.conn.commit()


    def get_user_details(self, username):
        '''Retrieves the value of a user row, only used in test suites'''
        user_row = self.curr.execute(''' SELECT * FROM users WHERE user_name =?;''',
                                     (username,)).fetchone()
        return user_row

    def get_all_users(self):
        '''Retrieves all usernames and emails from the system, restricted to admins'''
        users = self.curr.execute(''' SELECT user_name, email
                                         FROM users''',).fetchall()
        return users

    def retrieve_article_exists(self, title):
        '''Checks if a provided article with a title exists in the database'''
        for _ in self.curr.execute(''' SELECT 1
                                        FROM articles
                                        WHERE title = ?;''',
                                   (title, )):
            return True
        return False

    def create_article(self, title, content, user):
        '''Creates an article given the inputs'''
        user_id = self.curr.execute(''' SELECT id FROM users WHERE user_name = ?;''',
                                    (user,)).fetchone()[0]

        # Creates the article in the articles table
        self.curr.execute(''' INSERT into articles(title, content, postchanged)
                              VALUES(?,?,?);''', (title, content, datetime.datetime.now()))

        # Gets the auto incrementing article_id
        article_id = self.curr.execute(''' SELECT id FROM articles WHERE title = ?;''',
                                       (title,)).fetchone()[0]

        # Inserts into posts joining table who wrote the article
        self.curr.execute(''' INSERT into posts(article_id, user_id)
                              VALUES(?,?);''', (article_id, user_id))

        self.conn.commit()

    def retrieve_all_articles(self):
        '''Creates an article given the inputs'''
        # Using fetchall since pagination is out of scope for this project.
        articles = self.curr.execute(''' SELECT articles.id, articles.title, articles.content,
                                         articles.postchanged, users.user_name
                                         FROM articles
                                         INNER JOIN posts ON articles.id = 
                                         posts.article_id
                                         INNER JOIN users ON users.id = 
                                         posts.user_id;''',).fetchall()
        return articles

    def retrieve_specific_article(self, article_id):
        '''Retrieves a specific article from it's id'''
        # Using fetchall since pagination is out of scope for this project.
        articles = self.curr.execute(''' SELECT articles.id, articles.title, articles.content,
                                        articles.postchanged, users.user_name
                                        FROM articles
                                        INNER JOIN posts ON articles.id = posts.article_id
                                        INNER JOIN users ON users.id = posts.user_id
                                        WHERE articles.id = ?;''', (article_id, )).fetchone()
        return articles


    def retrieve_article_exists_id(self, article_id):
        '''Checks if a provided article id exists in the database'''
        for _ in self.curr.execute(''' SELECT 1
                                        FROM articles
                                        WHERE id = ?;''',
                                   (article_id, )):
            return True
        return False

    def update_article(self, article_id, title, content):
        '''Checks if a provided article id exists in the database'''
        print(id, title, content)
        self.curr.execute(''' UPDATE articles
                              SET title = ?,
                              content = ?,
                              postchanged = ?
                              WHERE id = ?;''',
                          (title, datetime.datetime.now(), content, article_id,))
        self.conn.commit()

    def retrieve_article_author(self, article_id):
        '''Takes an article's id value, returns it's author'''
        author = self.curr.execute(''' SELECT users.user_name
                                        FROM users
                                        INNER JOIN posts ON users.id = posts.user_id
                                        WHERE posts.article_id = ?;''', (article_id, )).fetchone()
        return author[0]

    def delete_article(self, article_id):
        '''Given a specific article ID, this article is deleted'''
        self.curr.execute(''' DELETE
                              FROM articles 
                              WHERE id =?;''', (article_id,))

        self.curr.execute(''' DELETE
                              FROM posts 
                              WHERE article_id =?;''', (article_id,))

        self.conn.commit()
