import {React, useState} from 'react';
import PropTypes from 'prop-types';
import {Button, Dialog, DialogContent, DialogTitle,
  DialogActions} from '@mui/material';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import LogoutIcon from '@mui/icons-material/Logout';
import HomeIcon from '@mui/icons-material/Home';
import {useNavigate} from 'react-router-dom';
import apiRequest from '../utils/api-request';
import EditUser from '../utils/edit-user';


/**
 * Template holder for pages elsewhere, wraps pages with navigation and log out.
 * @return {JSX.element}
 * @param {any} props contains the child component to render
 */
function PageTemplate(props) {
  // Enables system navigation
  const navigate = useNavigate();
  // Variables for system functionality
  const [edit, setEdit] = useState(false);
  const [alert, setAlert] = useState(false);

  // Handler for showing the self edit dialog
  const handleEdit = () =>{
    setEdit(!edit);
  };

  // Toggle for error dialog on sign out or edit failure
  const handleDialog = () =>{
    setAlert(false);
  };

  // Handler for logout
  const handleLogOut = async (event) =>{
    // Performs request to logout api, sets alert if an error occurs,
    // otherwise clears the localstorage and returns to log in page
    const resp = await apiRequest('POST', null, '/users/logout');
    if (resp.statusCode != 200) {
      setAlert(true);
    } else {
      localStorage.clear();
      navigate('/');
    }
  };

  // Handler for home button
  const handleHome = (event) =>{
    // Navigates users to the home page
    navigate('/home');
  };

  return (
    <div className="Page">
      <div>
        {/* Button for going to homepage */}
        <Button
          onClick={handleHome}
          variant='contained'
          sx={{mt: 3, mb: 2}}
          startIcon={<HomeIcon />}
        >
          Home
        </Button>
        {/* Button for logout */}
        <Button
          onClick={handleLogOut}
          variant='contained'
          sx={{mt: 3, mb: 2}}
          startIcon={<LogoutIcon />}
        >
          Logout
        </Button>
        {/* Button for user editting */}
        <Button
          onClick={handleEdit}
          variant='contained'
          sx={{mt: 3, mb: 2}}
          startIcon={<ManageAccountsIcon />}
        >
          Edit Profile
        </Button>
      </div>
      <h1>Library App</h1>
      {/* Renders the child component passed in */}
      {props.child}

      {/* Edit user dialog; appears if edit state variable is set to true */}
      <EditUser dialogOpenState={edit} dialogOpenStateFunc={handleEdit}
        user={localStorage.getItem('username')} admin={false}/>

      {/* Error dialog, appears if error occurs */}
      <Dialog open={alert} >
        <DialogTitle>An error ocurred</DialogTitle>
        <DialogContent>
        An error ocurred when making your request, please try again.
        If this persists, please message an administrator.
        </DialogContent>
        <DialogActions>
          {/* Closes error dialog */}
          <Button onClick={handleDialog}>Close</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

// Defines the parameter being passed in to be another element
PageTemplate.propTypes = {
  child: PropTypes.element,
};

export default PageTemplate;
