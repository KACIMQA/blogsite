import {React, useState, useEffect} from 'react';
import apiRequest from '../../utils/api-request';
import MaterialTable from 'material-table';
import EditUser from '../../utils/edit-user';


/**
 * Administration Page, displayers users in a material table
 * @return {JSX.element}
 */
export default function AdministrationPage() {
  // State variables for this page
  const [data, setData] = useState([]);
  const [edit, setEdit] = useState(false);
  const [username, setUsername] = useState('');

  // Gets all the users on page render, [] prevents infinite re-renders
  useEffect(()=>{
    getUsers();
  }, []);

  // Handle function for opening/closing edit dialog
  const handleEdit = () =>{
    setEdit(!edit);
  };

  // Handles which user has been selected
  const handleSelection = (user) =>{
    setUsername(user);
    handleEdit();
  };

  // Pulls users from the API
  const getUsers = async () => {
    // Performs POST request on API route
    const resp = await apiRequest('POST',
        {},
        '/users/admin');
    // If a non 200 status is returns, an error dialog is shown
    if (resp.statusCode != 200) {
      setAlert(true);
    } else {
      // Maps the database responses into a format for ingest by material table
      const usersArr = resp.data.msg.map((user) => {
        return {username: user[0], email: user[1]};
      });
      // Sets the mapped responses so material table can load the data
      setData(usersArr);
    }
  };

  return (
    <div>
      {/* Gets the material icons used by material table*/}
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/icon?family=Material+Icons"
      />
      <p>Administration page</p>
      {/* Generates a table with the correct data and edit actions */}
      <MaterialTable
        title="Users"
        columns={[
          {title: 'Username', field: 'username'},
          {title: 'Email', field: 'Email'},
        ]}
        data = {data}
        actions={[
          {
            icon: 'manage_accounts',
            tooltip: 'Edit User',
            onClick: (event, rowData) => handleSelection(rowData.username),
          },
        ]}
      />

      {/* Dialog that contains the edit user form, toggles on state*/}
      <EditUser dialogOpenState={edit} dialogOpenStateFunc ={handleEdit}
        user={username} admin={true}/>
    </div>
  );
}
