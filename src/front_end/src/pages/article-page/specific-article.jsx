import {Typography, Dialog, DialogTitle, Button,
  DialogActions, DialogContent, Card,
  CardContent, Box, TextField, Alert} from '@mui/material';
import {React, useState, useEffect} from 'react';
import {useParams} from 'react-router-dom';
import apiRequest from '../../utils/api-request';


/**
 * Loads and renders a specific given article
 * @return {JSX.element}
 */
export default function SpecificArticle() {
  // Various state variables used in below functionality
  const [alert, setAlert] = useState(false);
  const [success, setSuccess] = useState(false);
  const [edit, setEdit] = useState(false);
  const [deleteArticle, setDeleteArticle] = useState(false);
  const [data, setData] = useState([]);
  // id gets the id of the article to be displayed from URL parameters
  const id = useParams().id;
  // Gets the current user's username and their role from local storage
  const username = localStorage.getItem('username');
  const role = localStorage.getItem('role');

  // Runs on component render, [] prevents infinite re-renders.
  useEffect(()=>{
    getData();
  }, []);

  // Closes alert dialog
  const handleDialog = () =>{
    setAlert(false);
  };

  // Toggle for the edit article dialog appearing
  const handleEdit = () =>{
    setEdit(!edit);
  };

  // Toggle for the delete dialog appearing
  const handleDelete = () =>{
    setDeleteArticle(!deleteArticle);
  };

  // Toggle for closing the success popup
  const handleSuccess = () =>{
    setSuccess(false);
  };

  // Gets the data for the article to be shown, sets the error alert to appear
  // if the request is unsuccessful, otherwise adds the data to page state
  const getData = async () => {
    const resp = await apiRequest('POST',
        {'id': parseInt(id)},
        '/articles/post');
    if (resp.statusCode != 200) {
      setAlert(true);
    } else {
      setData(resp.data.msg);
    }
  };

  // Handler for article being editted, gets inputs from provided form event
  // if the request is unsuccessful, the error alert appears
  const handleSubmitEdit = async (event) => {
    event.preventDefault();
    const updateData = new FormData(event.currentTarget);
    const resp = await apiRequest('POST',
        {title: updateData.get('title'),
          content: updateData.get('content'),
          id: parseInt(id),
        },
        '/articles/update');
    if (resp.statusCode != 200) {
      setAlert(true);
    } else {
      setSuccess(true);
    }
  };

  // Handler for article being deleted, if the request is unsuccessful,
  // the error alert appears
  const handleSubmitDelete = async (event) => {
    event.preventDefault();
    const resp = await apiRequest('POST',
        {
          id: parseInt(id),
        },
        '/articles/delete');
    if (resp.statusCode != 200) {
      setAlert(true);
    } else {
      setSuccess(true);
    }
  };

  // returns the article to be shown
  return (
    <div>
      {/* data[0] is the article ID, data[1] it's title, data[2] it's content
          data[3] when it was posted and data[4] it's author
      */}
      <Card key ={data[0]} variant='outlined' >
        <CardContent>
          <Typography variant = 'h5'>{data[1]}</Typography>
          <Typography>{data[2]}</Typography>
          <Typography>Posted by: {data[4]}</Typography>
          <Typography>Posted at: {data[3]}</Typography>
          {/* Show edit and delete options if the user is the author or admin*/}
          {(username == data[4]||role =='admin') && <div>
            <Button
              fullWidth
              variant='contained'
              sx={{mt: 3, mb: 2}}
              onClick={handleEdit}
            >Edit article</Button>
            <Button
              fullWidth
              variant='contained'
              sx={{mt: 3, mb: 2}}
              onClick={handleDelete}
            >Delete article</Button>
          </div>}
        </CardContent>
      </Card>

      {/* Dialog appears when error occurs, otherwise stays hidden*/}
      <Dialog open={alert} >
        <DialogTitle>An error ocurred</DialogTitle>
        <DialogContent>
        An error ocurred when making your request, please try again.
        If this persists, please message an administrator.
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialog}>Close</Button>
        </DialogActions>
      </Dialog>

      {/* Dialog appears when edit button is pressed, otherwise stays hidden*/}
      <Dialog open={edit}>
        {/* Success message appears if edit was successful */}
        {success === true &&
            <Alert severity='success' onClose={handleSuccess}>
              Aricle Successfully Editted, refresh to see updates.
            </Alert>
        }
        <DialogContent>
          {/* Form component for gathering inputs*/}
          <Box component='form' onSubmit={handleSubmitEdit} sx={{mt: 1}}>
            <TextField
              margin='normal'
              required
              fullWidth
              id='title'
              label='Article Title'
              name='title'
            />
            <TextField
              margin='normal'
              required
              fullWidth
              name='content'
              label='Aritlce Content'
              id='content'
              multiline
            />
            <Button
              type='submit'
              fullWidth
              variant='contained'
              sx={{mt: 3, mb: 2}}
            >
              Update article
            </Button>
            {/* Cancel button to make dialog dissapear*/}
            <Button
              onClick={handleEdit}
              fullWidth
              variant='contained'
              sx={{mt: 3, mb: 2}}
            >
              Cancel
            </Button>
          </Box>
        </DialogContent>
      </Dialog>

      {/* Dialog appears when delete is pressed, otherwise stays hidden*/}
      <Dialog open={deleteArticle}>
        {success === true &&
            // Success message appears if delete was successful
            <Alert severity='success' onClose={handleSuccess}>
              Aricle Successfully deleted
            </Alert>
        }
        <DialogContent>
          {/* Form for consistency with above, pushes delete request to API
          when delete is confirmed */}
          <Box component='form' onSubmit={handleSubmitDelete} sx={{mt: 1}}>
            <Button
              type='submit'
              fullWidth
              variant='contained'
              sx={{mt: 3, mb: 2}}
            >
              Confirm Deletion
            </Button>
            <Button
              onClick={handleDelete}
              fullWidth
              variant='contained'
              sx={{mt: 3, mb: 2}}
            >
              Cancel
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
    </div>
  );
}
