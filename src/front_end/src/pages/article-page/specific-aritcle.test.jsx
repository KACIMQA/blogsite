import React from 'react';
import SpecificArticle from './specific-article';
import {mount} from 'enzyme';
import {Dialog} from '@mui/material';
import {act} from 'react-dom/test-utils';

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({success: true, msg: [1,
      'Test Title', 'Test Data', '2022-02-26 08:35:49.678406', 'username']}),
    status: 200,
  }));

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // use actual for all non-hook
  useParams: () => ({
    id: '1',
  }),
  useRouteMatch: () => ({url: '/articles/1'}),
}));

describe('Page-template components tests', ()=>{
  let wrapper;
  act(() => {
    wrapper = mount(<SpecificArticle />);
  });

  it('contains the text posted by', ()=>{
    console.log(wrapper.debug());
    expect(wrapper.text().includes('Posted by')).toBeTruthy();
  });

  it('contains the text posted at', ()=>{
    console.log(wrapper.debug());
    expect(wrapper.text().includes('Posted at')).toBeTruthy();
  });

  it('Finds a dialog', ()=>{
    expect(wrapper.find(Dialog)).toBeTruthy();
  });
});
