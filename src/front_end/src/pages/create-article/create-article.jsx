import {React, useState, useEffect} from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import RateReview from '@mui/icons-material/RateReview';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import apiRequest from '../../utils/api-request';
import Alert from '@mui/material/Alert';
import {Dialog, DialogTitle, DialogContent, DialogActions} from '@mui/material';

/**
 * Creates a create article page
 * @return {JSX.element}
 * @return {any} component for create article page
 */
export default function CreateArticle() {
  // state variables for page functionality
  const [alert, setAlert] = useState(false);
  const [success, setSuccess] = useState(false);

  useEffect(()=>{
    // Intentionally blank so page doesn't re-render on alert pop up
  }, []);

  // Handle form completion
  const handleSubmit = async (event) => {
    event.preventDefault();
    // Gets input data from form event
    const data = new FormData(event.currentTarget);
    // Makes response, causes error dialog if unsuccessful
    const resp = await apiRequest('POST',
        {title: data.get('title'),
          content: data.get('content')},
        '/articles/create');
    if (resp.statusCode != 200) {
      setAlert(true);
    } else {
      setSuccess(true);
    }
  };

  // Close error dialog
  const handleDialog = () =>{
    setAlert(false);
  };

  // Close success message
  const handleAlert = () =>{
    setSuccess(false);
  };

  return (
    <Container component='main' maxWidth='l'>
      <CssBaseline />
      {/* Renders a form for user inputs */}
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        {success === true &&
          // Success message appears if request was successful
          <Alert severity="success" onClose={handleAlert}>
            Aritcle Successfully Posted
          </Alert>
        }
        <RateReview />
        <Typography component='h1' variant='h4'>
            Create a new article.
        </Typography>
        <Box component='form' onSubmit={handleSubmit} sx={{mt: 1}}>
          <TextField
            margin='normal'
            required
            fullWidth
            id='title'
            label='Article Title'
            name='title'
          />
          <TextField
            margin='normal'
            required
            fullWidth
            name='content'
            label='Aritlce Content'
            id='content'
            multiline
          />
          <Button
            type='submit'
            fullWidth
            variant='contained'
            sx={{mt: 3, mb: 2}}
          >
            Post article
          </Button>
        </Box>
      </Box>
      {/* Error dialog; remains hidden unless error occurs*/}
      <Dialog open={alert} >
        <DialogTitle>An error ocurred</DialogTitle>
        <DialogContent>
          An error ocurred when making your request, please try again.
          If this persists, please message an administrator.
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialog}>Close</Button>
        </DialogActions>
      </Dialog>
    </Container>
  );
}
