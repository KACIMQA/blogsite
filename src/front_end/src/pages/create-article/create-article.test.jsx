import React from 'react';
import CreateArticle from './create-article';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import RateReview from '@mui/icons-material/RateReview';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {Dialog} from '@mui/material';
import {mount} from 'enzyme';

describe('Registerpage components rendering tests', ()=>{
  const wrapper = mount(<CreateArticle/>);
  it('Wraps components in a container', ()=>{
    expect(wrapper.find(Container).exists()).toBeTruthy();
  });

  it('Has two input fields', ()=>{
    expect(wrapper.find(TextField)).toHaveLength(2);
  });

  it('Has a button', ()=>{
    expect(wrapper.find(Button).exists()).toBeTruthy();
  });

  it('Shows a RateReview icon', ()=>{
    expect(wrapper.find(RateReview).exists()).toBeTruthy();
  });

  it('Has a text field', ()=>{
    expect(wrapper.find(Typography).exists()).toBeTruthy();
  });
});

// testing functionality of it rejecting presence should not be
// tested as this is pre-provided functionality
describe('Registerpage components integration tests', ()=>{
  it('Fills the two input fields and presses submit', ()=>{
    const wrapper = mount(<CreateArticle/>);
    wrapper.find('input').at(0).simulate('change', {target:
      {value: 'example title'}});
    wrapper.find('textarea').at(0).simulate('change', {target:
      {value: 'example content'}});
    wrapper.find('form').simulate('submit');
    setTimeout(()=>{}, 2000);
    expect(wrapper.find(Dialog).exists()).toBeTruthy();
  });
});
