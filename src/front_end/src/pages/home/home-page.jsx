import {Typography, Dialog, DialogTitle, Button,
  DialogActions, DialogContent, Card,
  CardContent, Link} from '@mui/material';
import {React, useState, useEffect} from 'react';
import apiRequest from '../../utils/api-request';

/**
 * Home page element
 * @return {JSX.element}
 */
export default function HomePage() {
  // alert and data variables for element functionality
  const [alert, setAlert] = useState(false);
  const [data, setData] = useState([]);

  // Gets all the articles to show users, [] prevents infinite rerenders
  useEffect(()=>{
    getData();
  }, []);

  // Toggles error dialog if error occurs
  const handleDialog = () =>{
    setAlert(false);
  };

  // Gets the articles data to show, makes error pop up appear if unsuccessful
  const getData = async () => {
    const resp = await apiRequest('POST',
        {},
        '/articles/');
    if (resp.statusCode != 200) {
      setAlert(true);
    } else {
      setData(resp.data.msg);
    }
  };

  return (
    <div>
      <p>Home page</p>
      {data.map((article) => (
        // Maps all of the articles returned to individual cards with the title
        // leading to a link to the specific article
        <Card key ={article[0]} variant="outlined" >
          <CardContent>
            <Link variant = "h5" href={'/article/'+article[0]}>
              {article[1]}
            </Link>
            <Typography>{article[2]}</Typography>
            <Typography>Posted at: {article[3]}</Typography>
          </CardContent>
        </Card>
      ))}
      {/* Error dialog, remains hidden until alert is set to open */}
      <Dialog open={alert} >
        <DialogTitle>An error ocurred</DialogTitle>
        <DialogContent>
          An error ocurred when making your request, please try again.
          If this persists, please message an administrator.
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialog}>Close</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
