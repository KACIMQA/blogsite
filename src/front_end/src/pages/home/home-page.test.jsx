import React from 'react';
import HomePage from './home-page';
import {mount} from 'enzyme';
import {Dialog} from '@mui/material';


describe('Page-template components tests', ()=>{
  const wrapper = mount(<HomePage />);
  it('contains a Home Page text components in a div', ()=>{
    expect(wrapper.find('Home Page')).toBeTruthy();
  });

  it('Finds a dialog', ()=>{
    expect(wrapper.find(Dialog)).toBeTruthy();
  });
});
