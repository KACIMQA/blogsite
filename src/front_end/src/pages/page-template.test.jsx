import React from 'react';
import PageTemplate from './page-template';
import {mount} from 'enzyme';

const mockedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedNavigate,
}));

describe('Page-template components tests', ()=>{
  const wrapper = mount(<PageTemplate />);
  it('contains a PageTemplate components in a div', ()=>{
    expect(wrapper.find(PageTemplate).exists()).toBeTruthy();
  });
});
