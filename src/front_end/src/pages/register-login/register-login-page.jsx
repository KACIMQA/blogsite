import {React, useState, useEffect} from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';
import LockIcon from '@mui/icons-material/Lock';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import {useNavigate} from 'react-router-dom';
import apiRequest from '../../utils/api-request';
import {Dialog, DialogTitle, DialogContent, DialogActions} from '@mui/material';
import PropTypes from 'prop-types';

/**
 * Creates a register or user page dependent on props
 * @return {JSX.element} component for register/landing page
 * @param {any} props indicates which page this renders as
 */
export default function RegisterLoginPage(props) {
  // Allows for routing between pages
  const navigate = useNavigate();
  // State used for error dialog
  const [alert, setAlert] = useState(false);

  useEffect(()=>{
    // Intentionally blank so page doesn't re-render on alert pop up
  }, []);
  // Handle form completion
  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    // Holding variable used for accessing the API Response further down
    let resp;
    // Performs request to either the register or login API depending on which
    // version of the page is being rendered.
    if (props.RegisterOrLogin == 'Register') {
      resp = await apiRequest('POST',
          {username: data.get('username'),
            email: data.get('email'),
            password: data.get('password')},
          '/users/register');
    } else {
      resp = await apiRequest('POST',
          {username: data.get('username'),
            password: data.get('password')},
          '/users/login');
    }
    // Shows alert dialog if error occurs
    if (resp.statusCode != 200) {
      setAlert(true);
    } else {
      if (props.RegisterOrLogin == 'Register') {
        // Navigates user to login page if they were on register and their
        // registration was successful
        navigate('/');
      } else {
        // Sets username and role in local storage. This can be overwritten
        // easily, this shouldn't be used for security.
        localStorage.setItem('username', data.get('username'));
        localStorage.setItem('role', resp.data.role);
        // Takes the user to the home page
        navigate('home');
      }
    }
  };

  // Sets variables as appropriate if the user is on the sign in or register
  // page
  let icon;
  let link;
  let text;
  if (props.RegisterOrLogin == 'Register') {
    icon = <GroupAddIcon />;
    link = <Link href='/' > Sign in page </Link>;
    text = 'Register';
  } else {
    icon = <LockIcon />;
    link = <Link href='/register' > Register for an account </Link>;
    text = 'Sign in';
  }

  const handleDialog = () =>{
    setAlert(false);
  };

  return (
    <Container component='main' maxWidth='xs'>
      {/* Wrappers for component sytling */}
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        {/* Variable for which UI icon to display */}
        {icon}
        <Typography component='h1' variant='h4'>
          {/* Shows text for the page headline */}
          {text}
        </Typography>
        {/* Form for submitting data, fields are added if in register mode */}
        <Box component='form' onSubmit={handleSubmit} sx={{mt: 1}}>
          <TextField
            margin='normal'
            required
            fullWidth
            id='username'
            label='Username'
            name='username'
            autoComplete='username'
          />
          {props.RegisterOrLogin == 'Register' &&
            <TextField
              margin='normal'
              required
              fullWidth
              id='email'
              label='Email Address'
              name='email'
              autoComplete='email'
            />
          }
          <TextField
            margin='normal'
            required
            fullWidth
            name='password'
            label='Password'
            type='password'
            id='password'
            autoComplete='off'
          />
          <Button
            type='submit'
            fullWidth
            variant='contained'
            sx={{mt: 3, mb: 2}}
          >
            {/* Sets the button text to match page mode */}
            {text}
          </Button>
          {/* Link to switch to other version of the page */}
          {link}
        </Box>
      </Box>
      {/* Error alert, appears if error occurs and is handled */}
      <Dialog open={alert} >
        <DialogTitle>An error ocurred</DialogTitle>
        <DialogContent>
          An error ocurred when making your request, please try again.
          If this persists, please message an administrator.
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialog}>Close</Button>
        </DialogActions>
      </Dialog>
    </Container>
  );
}

// Definition for the parameters being passed into this element
RegisterLoginPage.propTypes = {
  RegisterOrLogin: PropTypes.string,
};
