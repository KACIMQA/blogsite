import React from 'react';
import RegisterLoginPage from './register-login-page';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import {Dialog} from '@mui/material';
import {mount} from 'enzyme';
import LockIcon from '@mui/icons-material/Lock';

const mockedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedNavigate,
}));

describe('Registerpage components rendering tests', ()=>{
  const wrapper = mount(<RegisterLoginPage RegisterOrLogin='Register'/>);
  it('Wraps components in a container', ()=>{
    expect(wrapper.find(Container).exists()).toBeTruthy();
  });

  it('Has two 3 input fields', ()=>{
    expect(wrapper.find(TextField)).toHaveLength(3);
  });

  it('Has a button', ()=>{
    expect(wrapper.find(Button).exists()).toBeTruthy();
  });

  it('Has a link', ()=>{
    expect(wrapper.find(Link).exists()).toBeTruthy();
  });

  it('Shows a group add icon', ()=>{
    expect(wrapper.find(GroupAddIcon).exists()).toBeTruthy();
  });

  it('Has a text field', ()=>{
    expect(wrapper.find(Typography).exists()).toBeTruthy();
  });
});

describe('Loginpage components rendering tests', ()=>{
  const wrapper = mount(<RegisterLoginPage RegisterOrLogin='Login'/>);
  it('Wraps components in a container', ()=>{
    expect(wrapper.find(Container).exists()).toBeTruthy();
  });

  it('Has two 2 input fields', ()=>{
    expect(wrapper.find(TextField)).toHaveLength(2);
  });

  it('Has a button', ()=>{
    expect(wrapper.find(Button).exists()).toBeTruthy();
  });

  it('Has a link', ()=>{
    expect(wrapper.find(Link).exists()).toBeTruthy();
  });

  it('Shows a lock icon', ()=>{
    expect(wrapper.find(LockIcon).exists()).toBeTruthy();
  });

  it('Has a text field', ()=>{
    expect(wrapper.find(Typography).exists()).toBeTruthy();
  });
});

// testing functionality of it rejecting presence should not be
// tested as this is pre-provided functionality
describe('Registerpage components integration tests', ()=>{
  it('Fills the two input fields and presses submit', ()=>{
    const wrapper = mount(<RegisterLoginPage RegisterOrLogin='Register'/>);
    wrapper.find('input').at(0).simulate('change', {target:
      {value: 'Username'}});
    wrapper.find('input').at(1).simulate('change', {target:
      {value: 'email@email.com'}});
    wrapper.find('input').at(2).simulate('change', {target:
        {value: 'password'}});
    wrapper.find('form').simulate('submit');
    setTimeout(()=>{}, 2000);
    expect(wrapper.find(Dialog).exists()).toBeTruthy();
  });
});
