import React from 'react';
import App from './App';
import {mount} from 'enzyme';
import {
  BrowserRouter as Router,
  Routes,
} from 'react-router-dom';
import RegisterLoginPage from './pages/register-login/register-login-page';

describe('App tests', ()=>{
  const wrapper = mount(<App/>);
  it('Contains a Router', ()=>{
    expect(wrapper.find(Router).exists()).toBeTruthy();
  });

  it('Contains a Routes', ()=>{
    expect(wrapper.find(Routes).exists()).toBeTruthy();
  });

  it('Contains routes to the LoginRegisterPage', ()=>{
    expect(wrapper.find(RegisterLoginPage).exists()).toBeTruthy();
  });
});
