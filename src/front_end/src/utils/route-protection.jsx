import {React} from 'react';
import {Navigate} from 'react-router-dom';
import PropTypes from 'prop-types';
/**
 * Route protector variables, admins can access any page so this overwrites,
 * otherwise the role of the user is compared against the role of the page.
 * This can fairly easily be bypassed, so this check is done on the API as well.
 * @param {any} props contains role restriction, redirect location and child
 * @return {JSX.element}
 */
function RouteProtection(props) {
  // Gets the role of the user
  const role = localStorage.getItem('role');
  // Enables admin access
  if (role =='admin') {
    return props.child;
  // Checks if the user should be able to access this page,
  // if not then it redirects them
  } else if (role !=props.role) {
    return <Navigate to={props.redirect} replace />;
  }
  // returns the wrapped component if the user is allowed to access this page
  return props.child;
}

// child is the component to be wrapped,
// role is the role to be checked against
// redirect is the link to which a user should be redirected after a bad check
RouteProtection.propTypes = {
  child: PropTypes.element,
  role: PropTypes.string,
  redirect: PropTypes.string,
};

export default RouteProtection;
