

/**
 * apiRequests contains a generic handler for making API requests
 * @param {string} requestMethod - string representing REST method of use.
 * @param {object} requestBody - Object containing parameters for request body.
 * @param {any} requestEndpoint - string representing request endpoint.
 * @return {object} object containing response body
 */
export default async function apiRequest(requestMethod, requestBody,
    requestEndpoint) {
  // Headers to make request with
  const headers = new Headers({'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': true,
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Methods': '*'});
  // Makes the request to the API with the suppleid input parameters
  const resp = await fetch('http://localhost:5000'+requestEndpoint, {
    method: requestMethod,
    headers: headers,
    credentials: 'include',
    body: JSON.stringify(requestBody),
    mode: 'cors',
  });

  // Maps the response to a more digestable format
  const respCode = await resp.status;
  const data = await resp.json();

  return {statusCode: respCode, data: data};
}
