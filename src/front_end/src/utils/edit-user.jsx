import {React, useState} from 'react';
import PropTypes from 'prop-types';
import {Button, Dialog, Alert, DialogContent, Box,
  TextField, DialogTitle, DialogActions} from '@mui/material';
import apiRequest from '../utils/api-request';


/**
 * Contains element for edit user dialog
 * @return {JSX.element}
 * @param {any} props takes a state and handler for dialog open/close
 */
function EditUser(props) {
  // State variables for request error occurence/success
  const [success, setSuccess] = useState(false);
  const [alert, setAlert] = useState(false);

  // Handler for closing error dialog
  const handleDialog = () =>{
    setAlert(false);
  };

  // Handler for success alert
  const handleSuccess = () =>{
    setSuccess(false);
  };

  // Handler for submitting edit user request
  const handleSubmitEdit = async (event) => {
    event.preventDefault();
    // Gathers request data from supplied form, checks if a field is blank,
    // if blank, then it's not added to the request body.
    const updateData = new FormData(event.currentTarget);

    const requestBody ={current_username: props.user};

    updateData.get('currentPassword') && (requestBody['current_password']=
      updateData.get('currentPassword'));

    updateData.get('newUsername') && (requestBody['new_username']=
      updateData.get('newUsername'));

    updateData.get('newEmail') && (requestBody['email']=
      updateData.get('newEmail'));

    updateData.get('newPassword') && (requestBody['new_password']=
      updateData.get('newPassword'));

    // Makes API request and sets error/success as appropriate
    const resp = await apiRequest('POST', requestBody, '/users/edit');
    if (resp.statusCode != 200) {
      setAlert(true);
    } else {
      setSuccess(true);
    }
  };

  return (
    <div className="Dialog">
      {/* Dialog containing edit user form*/}
      <Dialog open={props.dialogOpenState}>
        {success === true &&
            <Alert severity='success' onClose={handleSuccess}>
              Profile editted. Re-sign in may be required.
            </Alert>
        }
        <DialogContent>
          {/* From component utilised with relevant text fields to send
          and gather inputs*/}
          <Box component='form' onSubmit={handleSubmitEdit} sx={{mt: 1}}>
            {/* If loading from the admin page, the current password is not
            required so it's not created*/}
            {!props.admin && <TextField
              margin='normal'
              required
              fullWidth
              name='currentPassword'
              label='Current Password (Required)'
              type='password'
              id='currentPassword'
              autoComplete='off'
            />}
            <TextField
              margin='normal'
              fullWidth
              name='newPassword'
              label='New Password (Not Required)'
              type='password'
              id='newPassword'
              autoComplete='off'
            />
            <TextField
              margin='normal'
              fullWidth
              id='newUsername'
              label='New Username (Not Required)'
              name='newUsername'
            />
            <TextField
              margin='normal'
              fullWidth
              id='newEmail'
              label='New Email (Not Required)'
              name='newEmail'
            />
            <Button
              type='submit'
              fullWidth
              variant='contained'
              sx={{mt: 3, mb: 2}}
            >
              Edit Profile Details
            </Button>
            {/* Closes the edit dialog using the parent component's state*/}
            <Button
              onClick={props.dialogOpenStateFunc}
              fullWidth
              variant='contained'
              sx={{mt: 3, mb: 2}}
            >
              Cancel
            </Button>
          </Box>
        </DialogContent>
      </Dialog>

      <Dialog open={alert} >
        {/* Error dialog, appears if error occurs */}
        <DialogTitle>An error ocurred</DialogTitle>
        <DialogContent>
        An error ocurred when making your request, please try again.
        If this persists, please message an administrator.
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialog}>Close</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

// Parameters for the page, takes a check for whether to show the edit form,
// a function to close the form,
// the username of the user to be editted
// and a boolean to determine whether the user is an admin.
EditUser.propTypes = {
  dialogOpenState: PropTypes.bool,
  dialogOpenStateFunc: PropTypes.func,
  user: PropTypes.string,
  admin: PropTypes.bool,
};

export default EditUser;
