import './App.css';
import {React} from 'react';
import HomePage from './pages/home/home-page';
import PageTemplate from './pages/page-template';
import SpecificArticle from './pages/article-page/specific-article';
import CreateArticle from './pages/create-article/create-article';
import RegisterLoginPage from './pages/register-login/register-login-page';
import AdministrationPage from './pages/administration/administration-page';
import RouteProtection from './utils/route-protection';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';

/**
 * App serves as the entry point to the system through creating a router, this
 * then handles linking between specific pages
 * @return {JSX.element}
 */
function App() {
  return (
    <Router>
      {/* Creates a router and enables the system to be navigable.*/}
      <div>
        <Routes>
          <Route path='/' element ={
            <RegisterLoginPage RegisterOrLogin='Login'/>} />

          <Route path='/register' element ={
            <RegisterLoginPage RegisterOrLogin='Register'/>} />
          {/* Route protection redirects users that aren't logged in to
          login page.*/}
          <Route path='/home' element={<RouteProtection role='user'
            redirect='/' child ={<PageTemplate child={
              <HomePage />} />}/>} />

          <Route path='/article/:id' element={<RouteProtection role='user'
            redirect='/' child ={<PageTemplate child={
              <SpecificArticle />} />}/>} />

          <Route path='/articles/create' element={<RouteProtection role='user'
            redirect='/' child ={<PageTemplate child={
              <CreateArticle />} />}/>} />
          {/* Route protection redirects users that aren't admins to
          home page. This page is only accessible via direct link.*/}
          <Route path='/administration' element={<RouteProtection role='admin'
            redirect='/home' child ={<AdministrationPage />}/>} />
        </Routes>
      </div>
    </Router>

  );
}

export default App;
